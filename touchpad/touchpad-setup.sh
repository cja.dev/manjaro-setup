# enable touchpad tapping and natural scrolling
# device: xinput list
# feature: xinput list-props <device>
# xinput set-prop <device> <feauture> <val>
#
xinput set-prop "DLL0A81:00 04F3:314B Touchpad" "libinput Tapping Enabled" 1 
xinput set-prop "DLL0A81:00 04F3:314B Touchpad" "libinput Tapping Enabled Default" 1
xinput set-prop "DLL0A81:00 04F3:314B Touchpad" "libinput Natural Scrolling Enabled" 1
xinput set-prop "DLL0A81:00 04F3:314B Touchpad" "libinput Natural Scrolling Enabled Default" 1
